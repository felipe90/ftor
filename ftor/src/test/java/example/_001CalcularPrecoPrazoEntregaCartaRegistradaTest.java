package example;

import org.testng.annotations.Test;
import ftor.ConfigureExecution;
import ftor.ReportLog;
import ftor.TestUtils;
import pages.PageConsultaPrecoPrazoEntrega;
import pages.PageResultadoConsultaPrecoPrazoEntrega;

public class _001CalcularPrecoPrazoEntregaCartaRegistradaTest extends TestUtils {

	// Caso de Teste
	@Test
	public void testar001() throws Exception {

		ConfigureExecution.configurarExecucaoCasosTeste("001");
	}

	// M�todo de Teste
	public void test001CalcularPrecoPrazoEntregaCartaRegistrada() throws Exception {

		/**********************
		 * Teste
		 ********************/
		PageConsultaPrecoPrazoEntrega page_consulta = new PageConsultaPrecoPrazoEntrega();
		PageResultadoConsultaPrecoPrazoEntrega page_resultado = new PageResultadoConsultaPrecoPrazoEntrega();
		ReportLog report = new ReportLog();

		page_consulta.acessarURLPrecoPrazoEntrega();
		page_consulta.informarCepOrigem();
		page_consulta.informarCepDestino();
		page_consulta.selecionarTipoServico();
		page_consulta.clicarBotaoEnviar();
		page_resultado.ValidarConsultaPrecoPrazoEntrega();

		report.adicionaImagemLogSucessoRelatorio("Evid�ncia de Teste: ");
	}
}

package example;

import org.testng.annotations.Test;
import ftor.ConfigureExecution;
import ftor.ReportLog;
import ftor.TestUtils;
import pages.PageConsultaPrecoPrazoEntrega;
import pages.PageResultadoConsultaPrecoPrazoEntrega;

public class _005CalcularPrecoPrazoEntregaCartaViaInternetTest extends TestUtils{
	
	@Test
	public void testar005() throws Exception {
		
		ConfigureExecution.configurarExecucaoCasosTeste("005");
	}
	
	public void test005CalcularPrecoPrazoEntregaCartaViaInternet() throws Exception {
		
		/**********************
		 * Teste
		 ********************/
		PageConsultaPrecoPrazoEntrega page_consulta = new PageConsultaPrecoPrazoEntrega();
		PageResultadoConsultaPrecoPrazoEntrega page_resultado = new PageResultadoConsultaPrecoPrazoEntrega();
		ReportLog report = new ReportLog();
		
		page_consulta.acessarURLPrecoPrazoEntrega();
		page_consulta.informarCepOrigem();
		page_consulta.informarCepDestino();
		page_consulta.selecionarTipoServico();
		page_consulta.clicarBotaoEnviar();
		page_resultado.ValidarConsultaPrecoPrazoEntrega();
		
		report.adicionaImagemLogSucessoRelatorio("EvidÍncia de Teste: ");
	}
}

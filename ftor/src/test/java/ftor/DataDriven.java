package ftor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/***********************************************************************************************
 * DataDriven -> Classe respons�vel pela manipula��o da massa de dados (CSV).
 ***********************************************************************************************/
public class DataDriven {

	public static String cCaso = null;
	public static String nomeCaso = null;
	public static String cLinha = null;
	public static int nLin = 0;
	public static int nLin2 = 0;
	public static String url = "http://www2.correios.com.br/sistemas/precosPrazos/";
	public static String nomeSistema = "Correios - Pre�os e Prazos"; 
	public static String nomeSuite = "C�lculo do Pre�o e Prazo de Entrega";

	public static String idCaso = null;
	public static String cepOrigem = null;
	public static String cepDestino = null;
	public static String tipoServico = null;
	public static String embalagem = null;
	public static String pesoEstimado = null;

	public static ArrayList<String> aLinha = new ArrayList<String>();
	public static ArrayList<String> aidCaso = new ArrayList<String>();
	public static ArrayList<String> aUrl = new ArrayList<String>();
	public static ArrayList<String> acepOrigem = new ArrayList<String>();
	public static ArrayList<String> acepDestino = new ArrayList<String>();
	public static ArrayList<String> atipoServico = new ArrayList<String>();
	public static ArrayList<String> aembalagem = new ArrayList<String>();
	public static ArrayList<String> apesoEstimado = new ArrayList<String>();

	
	public static void lerCSV() throws Exception {

		// Leitura do CSV
		File arquivoCSV = new File("dados/MASSA_ProjetoExemplo.csv");
		FileInputStream arquivoStream = new FileInputStream(arquivoCSV);
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(arquivoStream, "ISO-8859-1"));
			while (in.ready()) {
				cLinha = in.readLine();

				if (cLinha != null) {
					String[] campos = cLinha.split(";");

					aLinha.add(cLinha);
					acepOrigem.add(campos[0]);
					acepDestino.add(campos[1]);
					atipoServico.add(campos[2]);
					aembalagem.add(campos[3]);
					apesoEstimado.add(campos[4]);
					aidCaso.add(campos[5]);
				}
			}

			in.close();
		}

		catch (final Exception e) {

			System.out.println("N�o foi poss�vel ler a planilha .csv");
			throw new Exception();
		}
	}
	
	public static void lerRegistro() {
		// LER REGISTRO (get do array)
		idCaso = aidCaso.get(nLin);
		cepOrigem = acepOrigem.get(nLin);
		cepDestino = acepDestino.get(nLin);
		tipoServico = atipoServico.get(nLin);
		embalagem = aembalagem.get(nLin);
		pesoEstimado = apesoEstimado.get(nLin);
	}
}

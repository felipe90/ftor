package ftor;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.log4j.Logger;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;

/***************************************************************************************************
 * ReportLog -> Classe que cont�m os m�todos respons�veis pela manipula��o do Extent Reports, Log4j e
 * iText
 ***************************************************************************************************/
public class ReportLog extends TestUtils {

	public static ExtentTest logger;
	public static ExtentReports report;
	static ATUTestRecorder recorder;

	public void logSucesso(String mensagem) {
		// Log4j
		Logger log = Logger.getLogger(getClass());
		log.info(mensagem);

		// Extent Reports
		logger.log(LogStatus.PASS, mensagem);
	}

	public void logErro(String mensagem, Exception e) throws Exception {
		// Log4j
		Logger log = Logger.getLogger(getClass());
		log.error(mensagem);
		log.info("***** End LOG4J *****" + "\r\n");

		// (Extent Reports) -> Adiciona screenshot ao relat�rio
		String imagem = Utils.screenShot(DataDriven.nomeCaso);
		logger.log(LogStatus.FAIL, e + logger.addScreenCapture(imagem));

		// Finaliza o teste
		report.endTest(logger);

		// Acrescenta o arquivo HTML com todos os testes finalizados
		report.flush();

		// Adiciona o relat�rio .pdf de erro
		gerarRelatorioPDFErro();

		throw new Exception();
	}

	public void apontarRelatorioHtml(String nomeCaso) throws IOException {
		/*************************************
		 * Configura o relat�rio ExtentReports
		 ***********************************/

		DataDriven.nomeCaso = nomeCaso;

		// Define o apontamento do relat�rio html
		report = new ExtentReports(
				Utils.userDir + "\\Reports\\ExtentReports\\" + DataDriven.nomeCaso + "_" + Utils.gerarDataHoraAtual() + "_"
						+ Utils.obterNomeBrowser() + "_" + Utils.obterVersaoBrowser() + "_" + Utils.osName + ".html");

		String informacoesCasoTeste = "Caso de Teste: " + DataDriven.nomeCaso + "  |" + "   Colaborador: "
				+ Utils.userName + "   |" + "   Browser: " + Utils.obterNomeBrowser() + "  |" + "   Vers�o: "
				+ Utils.obterVersaoBrowser() + "  |" + "   S.O: " + Utils.osName;

		// Define o cabe�alho do relat�rio
		logger = report.startTest(informacoesCasoTeste);

		/************
		 * LOG4J
		 ************/
		// Loga texto no Log4j
		Logger log = Logger.getLogger(getClass());
		log.info("***** Start LOG4J *****");
		log.info(informacoesCasoTeste);
	}

	public void adicionaImagemLogSucessoRelatorio(String mensagemSucesso) throws Exception {

		// Pula uma linha no Log4j (separa��o entre os casos de teste)
		Logger log = Logger.getLogger(getClass());
		log.info("***** End LOG4J *****" + "\r\n");

		// Adiciona screenshot ao relat�rio
		String imagem = Utils.screenShot(DataDriven.nomeCaso);
		logger.log(LogStatus.PASS, mensagemSucesso + logger.addScreenCapture(imagem));

		// Finaliza o teste
		report.endTest(logger);

		// Acrescenta o arquivo HTML com todos os testes finalizados
		report.flush();

		// Adiciona o relat�rio .pdf de sucesso
		gerarRelatorioPDFSucesso();
	}

	public void logStatusPassRelatorioHtml(String mensagemSucesso) {
		logger.log(LogStatus.PASS, mensagemSucesso);
	}

	public void logStatusFailRelatorioHtml(String mensagemFalha) {
		logger.log(LogStatus.FAIL, mensagemFalha);
	}

	public void gerarRelatorioPDFSucesso() throws Exception {

		// Cria o objeto 'document'que � o arquivo .pdf
		Document document = new Document(PageSize.A4.rotate());

		try {

			// Configura o path do relat�rio .pdf e da evid�ncia de teste (concatenando com
			// o nome do caso de teste e o objeto do tipo Date criado anteriormente)
			String File = Utils.userDir + "\\Reports\\PDFReport\\" + DataDriven.nomeCaso + "_" + Utils.gerarDataHoraAtual() + "_"
					+ Utils.obterNomeBrowser() + "_" + Utils.obterVersaoBrowser() + "_" + Utils.osName + ".pdf";

			String IMG = Utils.userDir + "\\Reports\\ExtentReports\\" + DataDriven.nomeCaso + "_" + Utils.gerarDataHoraAtual()
					+ "_" + Utils.obterNomeBrowser() + "_" + Utils.obterVersaoBrowser() + "_" + Utils.osName + ".jpg";

			String IMGEmpresa = Utils.userDir + "\\Reports\\PDFReport\\logo-empresa\\correios.png";

			// Define a fonte que ser� utilizada na cria��o do checkbox de valida��o do
			// resultado do caso de teste
			BaseFont base = BaseFont.createFont("C:\\Windows\\Fonts\\wingding.ttf", BaseFont.IDENTITY_H, false);
			Font font = new Font(base, 16f, Font.BOLD, BaseColor.GREEN);
			char checked = '\u00FE';

			// Realiza open no arquivo .pdf
			PdfWriter.getInstance(document, new FileOutputStream(File));
			document.open();

			// Configura e adiciona a imagem (evid�ncia de teste), gerada na execu��o do
			// caso de teste
			Image img = Image.getInstance(IMG);
			img.setAlignment(Image.MIDDLE);
			img.scaleToFit(310, 310);
			float offsetX = (310 - img.getScaledWidth()) / 2;
			float offsetY = (310 - img.getScaledHeight()) / 2;
			img.setAbsolutePosition(36 + offsetX, 36 + offsetY);
			document.add(img);

			/*// Configura e adiciona a imagem (evid�ncia de teste), gerada na execu��o do
			// caso de teste
			Image img = Image.getInstance(IMG);
			img.setAlignment(Image.MIDDLE);
			img.scaleToFit(420, 320);
			float offsetX = (420 - img.getScaledWidth()) / 2;
			float offsetY = (320 - img.getScaledHeight()) / 2;
			img.setAbsolutePosition(36 + offsetX, 36 + offsetY);
			document.add(img);*/

			// Configura e adiciona o logo dos Correios
			Image imagem = Image.getInstance(IMGEmpresa);
			imagem.setAlignment(Image.RIGHT);
			document.add(imagem);

			// Adiciona o texto em quest�o centralizado
			Paragraph p = new Paragraph("RESUMO DA EXECU��O DE CASO DE TESTE");
			p.setAlignment(1);
			document.add(p);

			// Configura e adiciona as bordas do relat�rio (seta a cor verde, pois, o caso
			// de teste passou)
			Rectangle rect = new Rectangle(840, 594);
			rect.enableBorderSide(1);
			rect.enableBorderSide(2);
			rect.enableBorderSide(4);
			rect.enableBorderSide(8);
			rect.setBorder(Rectangle.BOX);
			rect.setBorderWidthBottom(4);
			rect.setBorderWidthLeft(4);
			rect.setBorderWidthRight(4);
			rect.setBorderWidthTop(4);
			rect.setBorderColor(BaseColor.GREEN);
			document.add(rect);

			// Configura a Data e Hora para posteriormente adicion�-la ao relat�rio
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Calendar cal = Calendar.getInstance();

			// Pula linha
			document.add(Chunk.NEWLINE);

			// Adiciona as informa��es do relat�rio
			document.add(new Paragraph("Sistema: " + DataDriven.nomeSistema));
			document.add(new Paragraph("Su�te de Teste: " + DataDriven.nomeSuite));
			document.add(new Paragraph("Caso de Teste: " + DataDriven.nomeCaso));
			document.add(new Paragraph("Colaborador: " + Utils.userName));
			document.add(new Paragraph("Status do Teste: Sucesso"));
			document.add(new Paragraph(String.valueOf(checked), font));

			// Pula linhas para adicionar a Data/Hora na parte inferior do arquivo .pdf
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);


			// Adiciona Data/Hora ao relat�rio
			document.add(new Paragraph("Data/Hora: " + dateFormat.format(cal.getTime())));


		} catch (Exception e) {
			logErro("Erro ao gerar o relat�rio .pdf", e);
		} finally {

			// Finaliza o relat�rio .pdf
			document.close();
		}
	}

	public void gerarRelatorioPDFErro() throws Exception {

		// Cria o objeto 'document'que � o arquivo .pdf
		Document document = new Document(PageSize.A4.rotate());

		try {

			// Configura o path do relat�rio .pdf e da evid�ncia de teste (concatenando com
			// o nome do caso de teste e o objeto do tipo Date criado anteriormente)
			String File = Utils.userDir + "\\Reports\\PDFReport\\" + DataDriven.nomeCaso + "_" + Utils.gerarDataHoraAtual() + "_"
					+ Utils.obterNomeBrowser() + "_" + Utils.obterVersaoBrowser() + "_" + Utils.osName + ".pdf";

			String IMG = Utils.userDir + "\\Reports\\ExtentReports\\" + DataDriven.nomeCaso + "_" + Utils.gerarDataHoraAtual()
					+ "_" + Utils.obterNomeBrowser() + "_" + Utils.obterVersaoBrowser() + "_" + Utils.osName + ".jpg";
			
			String IMGEmpresa = Utils.userDir + "\\Reports\\PDFReport\\logo-empresa\\correios.png";

			// Define a fonte que ser� utilizada na cria��o do checkbox de valida��o do
			// resultado do caso de teste
			BaseFont base = BaseFont.createFont("C:\\Windows\\Fonts\\wingding.ttf", BaseFont.IDENTITY_H, false);
			Font font = new Font(base, 16f, Font.BOLD, BaseColor.RED);
			char checked = '\u00FE';

			// Realiza open no arquivo .pdf
			PdfWriter.getInstance(document, new FileOutputStream(File));
			document.open();

			// Configura e adiciona a imagem (evid�ncia de teste), gerada na execu��o do
			// caso de teste
			Image img = Image.getInstance(IMG);
			img.setAlignment(Image.MIDDLE);
			img.scaleToFit(310, 310);
			float offsetX = (310 - img.getScaledWidth()) / 2;
			float offsetY = (310 - img.getScaledHeight()) / 2;
			img.setAbsolutePosition(36 + offsetX, 36 + offsetY);
			document.add(img);

			// Configura e adiciona o logo dos Correios
			Image imagem = Image.getInstance(IMGEmpresa);
			imagem.setAlignment(Image.RIGHT);
			document.add(imagem);

			// Configura e adiciona as bordas do relat�rio (seta a cor vermelha, pois, o
			// caso
			// de teste falhou)
			Rectangle rect = new Rectangle(840, 594);
			rect.enableBorderSide(1);
			rect.enableBorderSide(2);
			rect.enableBorderSide(4);
			rect.enableBorderSide(8);
			rect.setBorder(Rectangle.BOX);
			rect.setBorderWidthBottom(4);
			rect.setBorderWidthLeft(4);
			rect.setBorderWidthRight(4);
			rect.setBorderWidthTop(4);
			rect.setBorderColor(BaseColor.RED);
			document.add(rect);

			// Configura a Data e Hora para posteriormente adicion�-la ao relat�rio
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Calendar cal = Calendar.getInstance();

			// Adiciona o texto em quest�o centralizado
			Paragraph p = new Paragraph("RESUMO DA EXECU��O DE CASO DE TESTE");
			p.setAlignment(1);
			document.add(p);

			// Pula linha
			document.add(Chunk.NEWLINE);

			// Adiciona as informa��es do relat�rio
			document.add(new Paragraph("Sistema: " + DataDriven.nomeSistema));
			document.add(new Paragraph("Su�te de Teste: " + DataDriven.nomeSuite));
			document.add(new Paragraph("Caso de Teste: " + DataDriven.nomeCaso));
			document.add(new Paragraph("Colaborador: " + Utils.userName));
			document.add(new Paragraph("Status do Teste: Falha"));
			document.add(new Paragraph(String.valueOf(checked), font));

			// Pula linhas para adicionar a Data/Hora na parte inferior do arquivo .pdf
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);

			// Adiciona Data/Hora ao relat�rio
			document.add(new Paragraph("Data/Hora: " + dateFormat.format(cal.getTime())));

		} catch (Exception e) {
			logErro("Erro ao gerar o relat�rio .pdf", e);
		} finally {

			// Finaliza o relat�rio .pdf
			document.close();
		}
	}

	public static void iniciarGravacaoVideoExecucaoCasoTeste() throws ATUTestRecorderException {
		recorder = new ATUTestRecorder(Utils.userDir + "\\Reports\\Video\\",
				DataDriven.nomeCaso + "_" + Utils.gerarDataHoraAtual() + "_" + Utils.obterNomeBrowser() + "_"
						+ Utils.obterVersaoBrowser() + "_" + Utils.osName,
				false);
		recorder.start();
	}

	public static void finalizarGravacaoVideoExecucaoCasoTeste() throws ATUTestRecorderException {
		recorder.stop();
	}
}

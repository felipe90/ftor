
package ftor;

import example._001CalcularPrecoPrazoEntregaCartaRegistradaTest;
import example._002CalcularPrecoPrazoEntregaCartaSimplesTest;
import example._003CalcularPrecoPrazoEntregaImpressoNormalTest;
import example._004CalcularPrecoPrazoEntregaPACTest;
import example._005CalcularPrecoPrazoEntregaCartaViaInternetTest;

/*******************************************************************************************************************************
 * ConfigureExecution -> Classe respons�vel por configurar a execu��o de cada caso de teste e pela leitura da massa de dados CSV
 *******************************************************************************************************************************/

public class ConfigureExecution {

	// Atributos
	public static String cCaso = "";
	public static int nLin = 0;
	public static int nLin2 = 0;

	// Objetos das classes de teste
	static ReportLog reportlog = new ReportLog();

	static _001CalcularPrecoPrazoEntregaCartaRegistradaTest ct001 = new _001CalcularPrecoPrazoEntregaCartaRegistradaTest();
	static _002CalcularPrecoPrazoEntregaCartaSimplesTest ct002 = new _002CalcularPrecoPrazoEntregaCartaSimplesTest();
	static _003CalcularPrecoPrazoEntregaImpressoNormalTest ct003 = new _003CalcularPrecoPrazoEntregaImpressoNormalTest();
	static _004CalcularPrecoPrazoEntregaPACTest ct004 = new _004CalcularPrecoPrazoEntregaPACTest();
	static _005CalcularPrecoPrazoEntregaCartaViaInternetTest ct005 = new _005CalcularPrecoPrazoEntregaCartaViaInternetTest();

	public static void configurarExecucaoCasosTeste(String idCaso) throws Exception {

		switch (idCaso) {

		case "001":

			cCaso = "001";

			DataDriven.lerCSV();

			try {
				reportlog.apontarRelatorioHtml("_001CalcularPrecoPrazoEntregaCartaRegistradaTest");
				processarCasosTeste(idCaso);
			} catch (final Exception e) {

				throw new Exception();
			}

			break;

		case "002":

			cCaso = "002";

			DataDriven.lerCSV();

			try {
				reportlog.apontarRelatorioHtml("_002CalcularPrecoPrazoEntregaCartaSimples");
				processarCasosTeste(idCaso);
			} catch (final Exception e) {

				throw new Exception();
			}

			break;

		case "003":

			cCaso = "003";

			DataDriven.lerCSV();

			try {
				reportlog.apontarRelatorioHtml("_003CalcularPrecoPrazoEntregaImpressoNormal");
				processarCasosTeste(idCaso);
			} catch (final Exception e) {

				throw new Exception();
			}

			break;

		case "004":

			cCaso = "004";

			DataDriven.lerCSV();

			try {
				reportlog.apontarRelatorioHtml("_004CalcularPrecoPrazoEntregaPAC");
				processarCasosTeste(idCaso);
			} catch (final Exception e) {

				throw new Exception();
			}

			break;
			
		case "005":

			cCaso = "005";

			DataDriven.lerCSV();

			try {
				reportlog.apontarRelatorioHtml("_005CalcularPrecoPrazoEntregaCartaViaInternet");
				processarCasosTeste(idCaso);
			} catch (final Exception e) {

				throw new Exception();
			}

			break;
		}
	}
		

	public static void processarCasosTeste(String idCaso) throws Exception {

		switch (idCaso) {

		case "001":

			// Verifica��o do id do caso
			nLin2 = DataDriven.aLinha.size() - 1;

			for (nLin = 1; nLin <= nLin2; nLin++)

			{
				if (cCaso.equalsIgnoreCase(DataDriven.aidCaso.get(nLin))) {
					DataDriven.nLin = nLin;
					DataDriven.nLin2 = nLin2;

					DataDriven.lerRegistro();

					// CHAMA M�TODO DE TESTE
					ReportLog.iniciarGravacaoVideoExecucaoCasoTeste();
					ct001.test001CalcularPrecoPrazoEntregaCartaRegistrada();
					break;
				}
			}

			break;

		case "002":

			// Verifica��o do id do caso
			nLin2 = DataDriven.aLinha.size() - 1;

			for (nLin = 1; nLin <= nLin2; nLin++)

			{
				if (cCaso.equalsIgnoreCase(DataDriven.aidCaso.get(nLin))) {
					DataDriven.nLin = nLin;
					DataDriven.nLin2 = nLin2;

					DataDriven.lerRegistro();

					// CHAMA M�TODO DE TESTE
					ReportLog.iniciarGravacaoVideoExecucaoCasoTeste();
					ct002.test002CalcularPrecoPrazoEntregaCartaSimples();
					break;
				}
			}

			break;

		case "003":

			// Verifica��o do id do caso
			nLin2 = DataDriven.aLinha.size() - 1;

			for (nLin = 1; nLin <= nLin2; nLin++)

			{
				if (cCaso.equalsIgnoreCase(DataDriven.aidCaso.get(nLin))) {
					DataDriven.nLin = nLin;
					DataDriven.nLin2 = nLin2;

					DataDriven.lerRegistro();

					// CHAMA M�TODO DE TESTE
					ReportLog.iniciarGravacaoVideoExecucaoCasoTeste();
					ct003.test003CalcularPrecoPrazoEntregaImpressoNormal();
					break;
				}
			}

			break;

		case "004":

			// Verifica��o do id do caso
			nLin2 = DataDriven.aLinha.size() - 1;

			for (nLin = 1; nLin <= nLin2; nLin++)

			{
				if (cCaso.equalsIgnoreCase(DataDriven.aidCaso.get(nLin))) {
					DataDriven.nLin = nLin;
					DataDriven.nLin2 = nLin2;

					DataDriven.lerRegistro();

					// CHAMA M�TODO DE TESTE
					ReportLog.iniciarGravacaoVideoExecucaoCasoTeste();
					ct004.test004CalcularPrecoPrazoEntregaPAC();
					break;
				}
			}

			break;
			
		case "005":

			// Verifica��o do id do caso
			nLin2 = DataDriven.aLinha.size() - 1;

			for (nLin = 1; nLin <= nLin2; nLin++)

			{
				if (cCaso.equalsIgnoreCase(DataDriven.aidCaso.get(nLin))) {
					DataDriven.nLin = nLin;
					DataDriven.nLin2 = nLin2;

					DataDriven.lerRegistro();

					// CHAMA M�TODO DE TESTE
					ReportLog.iniciarGravacaoVideoExecucaoCasoTeste();
					ct005.test005CalcularPrecoPrazoEntregaCartaViaInternet();
					break;
				}
			}

			break;
		}
	}
}

package ftor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;
import org.apache.log4j.Logger;

import com.relevantcodes.extentreports.LogStatus;

import ftor.ReportLog;

import org.openqa.selenium.TakesScreenshot;

/***********************************************************************************
 * Utils -> Classe respons�vel por conter atributos e m�todos auxiliares que
 * ser�o utilizados na implementa��o das pages
 ***********************************************************************************/
public class Utils extends ReportLog {

	public static String userDir = System.getProperty("user.dir");
	public static String userName = System.getProperty("user.name");
	public static String osName = System.getProperty("os.name");
	public static String osVersion = System.getProperty("os.version");
	public static String osArchitecture = System.getProperty("os.arch");

	// Aceita o alerta (clica no bot�o OK).
	public void aceitarAlerta() throws Exception {
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
			logSucesso("Aceita o alert (clica no bot�o 'OK')");
		} catch (Exception e) {
			logErro("N�o foi poss�vel aceitar o alert", e);
		}
	}

	// Acessa � URL passada por par�metro.
	public void acessarURL(String url) throws Exception {
		try {
			driver.get(url);
			logSucesso("Acessa a URL " + url);
		} catch (Exception e) {
			logErro("N�o foi poss�vel acessar a URL " + url, e);
		}
	}

	/*
	 * Aguarda o alert ser exibido (wait din�mico). Quando o mesmo estiver vis�vel,
	 * o teste ir� prosseguir. O tempo limite � de 30 segundos.
	 */
	public void aguardarPorAlert() throws Exception {
		long tempoEspera = 30;
		try {
			WebDriverWait wait = new WebDriverWait(driver, tempoEspera);
			Alert element = wait.until(ExpectedConditions.alertIsPresent());
		} catch (Exception e) {
			logErro("Timeout por aguardar pela presen�a do alert por " + tempoEspera + " segundos", e);
		}
	}

	/*
	 * Aguarda o elemento ser exibido (wait din�mico). Quando o mesmo estiver
	 * vis�vel, o teste ir� prosseguir. O tempo limite � de 30 segundos.
	 */
	public void aguardarPorElemento(By by) throws Exception {
		long tempoEspera = 30;
		try {
			WebDriverWait wait = new WebDriverWait(driver, tempoEspera);
			WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			logErro("Timeout por aguardar pela presen�a do elemento " + by + " por " + tempoEspera + " segundos", e);
		}
	}

	// Altera a janela corrente do teste pelo t�tulo da p�gina passado por
	// par�metro.
	public void alterarJanelaPorTituloPagina(String titulo) throws Exception {
		try {
			for (String winHandle : driver.getWindowHandles()) {
				if (driver.switchTo().window(winHandle).getTitle().equals(titulo)) {
					break;
				}
			}

			pausar(2000);
			logSucesso("Altera o foco para janela com o t�tulo " + titulo);
		} catch (Exception e) {
			logErro("N�o foi poss�vel alterar o foco para a janela com o t�tulo " + titulo, e);
		}
	}

	// Altera a janela corrente do teste pela URL da p�gina passada por
	// par�metro.
	public void alterarJanelaPorURL(String url) throws Exception {
		try {
			for (String winHandle : driver.getWindowHandles()) {
				if (driver.switchTo().window(winHandle).getCurrentUrl().equals(url)) {
					break;
				}
			}

			pausar(2000);
			logSucesso("Altera o foco para janela com a URL " + url);
		} catch (Exception e) {
			logErro("N�o foi poss�vel alterar o foco para janela com a URL " + url, e);
		}
	}

	// Altera o foco para o frame 'ifrm'
	public void alterarFocoFrame() throws Exception {
		String frame = "ifrm";
		int frameZero = 0;

		try {
			driver.switchTo().frame(frame);
			logSucesso("Altera o foco para o frame " + frame);
		} catch (NoSuchElementException e) {
			driver.switchTo().frame(frameZero);
			logErro("N�o foi poss�vel alterar o foco para o frame " + frame + ",o foco foi alterado para o frame "
					+ frameZero, e);
		}
	}

	// Arrasta elemento e solta em um determinado local
	public void arrastarSoltarElemento(By elemento, By localFuturo) throws Exception {
		try {
			WebElement sourceLocator = driver.findElement(elemento);
			WebElement destinationLocator = driver.findElement(localFuturo);

			Actions actions = new Actions(driver);
			actions.dragAndDrop(sourceLocator, destinationLocator).build().perform();

			logSucesso("Arrasta elemento " + elemento + " e solta no local " + localFuturo);
		} catch (Exception e) {
			logErro("N�o foi poss�vel arrastar elemento " + elemento + "e soltar no local " + localFuturo, e);
		}
	}

	// Busca por um texto na p�gina atual
	public boolean buscarPorTextoNaPagina(String texto) throws Exception {
		try {
			boolean result = driver.getPageSource().contains(texto);
			logSucesso("Foi o encontrado o texto " + texto + " na p�gina atual: " + driver.getCurrentUrl());
			return true;
		} catch (Exception e) {
			logErro("N�o foi poss�vel encontrar na p�gina atual " + driver.getCurrentUrl() + ", o texto " + texto, e);
			return false;
		}
	}

	/*
	 * Busca valores na combobox de acordo com a op��o desejada. N�o importa a
	 * posi��o em que o valor encontra-se, a busca ser� feita pelo valor desejado na
	 * combobox.
	 */
	public void buscarValoresNaComboBoxPorTexto(By cliqueSimplesNaCombo, By xpathDaListaElementos,
			String valorDesejadoNaCombo) throws Exception {
		try {
			WebElement findElement = driver.findElement((cliqueSimplesNaCombo));
			findElement.click();

			List<WebElement> elementos = driver.findElements((xpathDaListaElementos));

			for (WebElement elementosGrid : elementos) {
				if (elementosGrid.getText().equals(valorDesejadoNaCombo)) {

					elementosGrid.click();
				}
			}
			logSucesso("Seleciona o valor " + valorDesejadoNaCombo + " no elemento " + xpathDaListaElementos);
		} catch (Exception e) {
			logErro("N�o foi poss�vel selecionar o valor " + valorDesejadoNaCombo + " no elemento "
					+ xpathDaListaElementos, e);
		}
	}

	// Clica no componente passado por par�metro.
	public void clicarElemento(By by, String nomeElemento) throws Exception {
		try {
			aguardarPorElemento(by);
			driver.findElement(by).click();
			logSucesso("Clica no elemento " + nomeElemento);
		} catch (Exception e) {
			logErro("N�o foi poss�vel clicar no elemento " + nomeElemento, e);
		}
	}

	// Verifica se a propriedade �text� do componente em quest�o cont�m o texto
	// passado por par�metro.
	public void contemTextoPresentePorElemento(By by, String texto, String nomeElemento) throws Exception {

		try {
			AssertJUnit.assertTrue(driver.findElement(by).getText().contains(texto));
			logSucesso("Valida que o elemento " + nomeElemento + " cont�m em sua propriedade 'text' o texto " + texto);
		} catch (Exception e) {
			logErro("N�o foi poss�vel validar que o elemento " + nomeElemento
					+ " contenha em sua propriedade 'text' o texto " + texto, e);
		}
	}

	// Deleta todos os cookies do navegador.
	public void deletarTodosCookies() {
		driver.manage().deleteAllCookies();
	}

	// N�o aceita o alerta (clica no bot�o Cancelar).
	public void dispensarAlerta() throws Exception {
		try {
			Alert alert = driver.switchTo().alert();
			alert.dismiss();
			logSucesso("Dispensa o alert (clica no bot�o 'Cancelar')");
		} catch (Exception e) {
			logErro("N�o foi poss�vel dispensar o alert", e);
		}
	}

	// Realiza duplo clique em um elemento
	public void duploCliqueElemento(By by, String nomeElemento) throws Exception {
		try {
			Actions action = new Actions(driver);
			WebElement element = driver.findElement(by);

			action.doubleClick(element).perform();

			logSucesso("Realiza duplo clique no elemento " + nomeElemento);
		} catch (Exception e) {
			logErro("N�o foi poss�vel realizar duplo clique no elemento " + by, e);
		}
	}

	// Verifica se o componente est� vis�vel. Propriedade �isDisplayed�.
	public boolean elementoEstaVisivel(By by) {
		try {
			return driver.findElement(by).isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	// Verifica se o componente n�o est� vis�vel. Propriedade �isDisplayed�.
	public boolean elementoNaoEstaVisivel(By by) {
		try {
			AssertJUnit.assertFalse(driver.findElement(by).isDisplayed());
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	// Verifica se o elemento est� presente.
	public boolean elementoEstaPresente(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	// Verifica se o elemento n�o est� presente.
	public void elementoNaoEstaPresente(By by) throws Exception {
		try {
			boolean notPresent = ExpectedConditions.not(ExpectedConditions.presenceOfElementLocated(by)).apply(driver);
			AssertJUnit.assertTrue(notPresent);
		} catch (Exception e) {
		}
	}

	// Preenche campo de texto (componente) com texto passado por par�metro.
	public void escreverTexto(By by, String texto, String nomeElemento) throws Exception {
		try {
			aguardarPorElemento(by);
			driver.findElement(by).clear();
			driver.findElement(by).sendKeys(texto);

			logSucesso("Preenche o campo de texto " + nomeElemento + " com o valor " + texto);
		} catch (Exception e) {
			logErro("N�o foi poss�vel preencher o campo de texto " + by, e);
		}
	}

	// Preenche campo de texto (componente) com texto passado por par�metro
	// utilizando comando javascript.
	public void escreverTextoComandoJavaScript(By by, String script, String texto, String nomeElemento)
			throws Exception {
		try {
			aguardarPorElemento(by);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript(script); // exemplo de script:
			// document.getElementById('form').style=''
			driver.findElement(by).clear();
			driver.findElement(by).sendKeys(texto);

			logSucesso("Preenche o campo de texto " + nomeElemento + " com o valor " + texto);
		} catch (Exception e) {
			logErro("N�o foi poss�vel preencher o campo de texto " + by, e);
		}
	}

	// Verifica se existe alerta sendo exibido no momento.
	public boolean existeAlerta() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	// Realiza o file upload de um arquivo buscando da pasta 'upload'
	public void fileUpload(By by, String arquivo, String nomeElemento) throws Exception {
		String pathUpload = (userDir + "\\upload\\" + arquivo);
		try {
			driver.findElement(by).clear();
			driver.findElement(by).sendKeys(pathUpload);

			logSucesso("Realiza upload do arquivo " + arquivo + " no campo " + nomeElemento);
		} catch (Exception e) {
			logErro("N�o foi poss�vel realizar o upload do arquivo " + pathUpload + " no campo " + by, e);
		}
	}

	// Retorna um n�mero rand�mico entre 0 e 100.
	public int gerarNumeroRandomico() {
		Random random = new Random();
		int x = random.nextInt(101);

		return x;
	}

	// Adiciona imagem ao campo 'Capturas de Tela' do log do RQM.
	public void gravarTelaRQM() {

		// Formata a data para utiliza��o no nome das screenshots dos passos do
		// caso de teste que ser�o gravados no log e no RQM.
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH");
		Date dDate = new Date();
		String cData = dateFormat.format(dDate);

		// RQM
		String cJpeg = DataDriven.nomeCaso + "_" + cData + ".jpg";

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String imageFileDir = System.getProperty("selenium.screenshot.dir");
		if (imageFileDir == null) {
			imageFileDir = System.getProperty("java.io.tmpdir");
		}

		try {
			FileUtils.copyFile(scrFile, new File(imageFileDir, cJpeg));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Limpa o campo de texto.
	public void limparCampoTexto(By by, String nomeElemento) throws Exception {
		try {
			aguardarPorElemento(by);
			driver.findElement(by).clear();
			logSucesso("Limpa o campo de texto " + nomeElemento);
		} catch (Exception e) {
			logErro("N�o foi poss�vel limpar o campo de texto " + by, e);
		}
	}

	// Realiza a a��o 'Mouse Over' no elemento
	public void mouseOverNoElemento(By by) throws Exception {
		try {
			Actions actions = new Actions(driver);
			WebElement mouseHover = driver.findElement(by);
			actions.moveToElement(mouseHover).perform();

			logSucesso("Realiza a a��o 'Mouse Over' no elemento " + by);
		} catch (Exception e) {
			logErro("N�o foi poss�vel realizar a a��o 'Mouse Over' no elemento " + by, e);
		}
	}

	// Realiza a a��o 'Mouse Scroll' de baixo para cima na p�gina
	public void mouseScrollBaixoParaCima(By by) throws Exception {
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,500)", "");

			logSucesso("Realiza a a��o 'Mouse Scroll' de baixo para cima na p�gina");
		} catch (Exception e) {
			logErro("N�o foi poss�vel realizar a a��o 'Mouse Scroll' de baixo para cima na p�gina", e);
		}
	}

	// Realiza a a��o 'Mouse Scroll' at� que o elemento seja visualizado
	public void mouseScrollBuscaElemento(By by) throws Exception {
		try {
			WebElement element = driver.findElement(by);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

			logSucesso("Realiza a a��o 'Mouse Scroll' at� que o elemento " + by + "seja visualizado");
		} catch (Exception e) {
			logErro("N�o foi poss�vel realizar a a��o 'Mouse Scroll' para visualizar o elemento " + by, e);
		}
	}

	// Realiza a a��o 'Mouse Scroll' de cima para baixo na p�gina
	public void mouseScrollCimaParaBaixo(By by) throws Exception {
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollTo(0,document.body.scrollHeight);");

			logSucesso("Realiza a a��o 'Mouse Scroll' de cima para baixo na p�gina");
		} catch (Exception e) {
			logErro("N�o foi poss�vel realizar a a��o 'Mouse Scroll' de cima para baixo na p�gina", e);
		}
	}

	// Pausa a execu��o do teste de acordo com o tempo (milissegundos) passado
	// por par�metro.
	public void pausar(int tempo) {
		try {
			Thread.sleep(tempo);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Pressiona tecla de teclado passando por par�metro.
	public void pressionarTecla(By by, Keys keyboard) throws Exception {

		aguardarPorElemento(by);
		driver.findElement(by).sendKeys(keyboard);
	}

	// Realiza refresh na p�gina.
	public void refreshPagina() {
		driver.navigate().refresh();
	}

	/*
	 * Tira uma screenshot e a armazena na pasta ExtentReports com o nome passado
	 * por par�metro concatenando com �yyyyMMddHH�, no formato jpg.
	 */
	public static String screenShot(String fileName) {

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		String imagem = fileName + "_" + Utils.gerarDataHoraAtual() + "_" + 
                Utils.obterNomeBrowser() + "_" + 
                Utils.obterVersaoBrowser() + "_" + 
                Utils.osName + ".jpg";

		try {

			FileUtils.copyFile(scrFile, new File("Reports/ExtentReports/" + imagem), true);
			return imagem;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return imagem;
	}

	// Seleciona op��o em combobox pelo texto passado por par�metro.
	public void selecionarElementoPorTexto(By by, String texto, String nomeElemento) throws Exception {
		try {
			aguardarPorElemento(by);
			new Select(driver.findElement(by)).selectByVisibleText(texto);

			logSucesso("Seleciona o texto " + texto + " no elemento " + nomeElemento);
		} catch (Exception e) {
			logErro("N�o foi poss�vel selecionar o texto " + texto + " no elemento " + by, e);
		}
	}

	// Seleciona op��o em combobox pela propriedade value passada por par�metro.
	public void selecionarElementoPorValue(By by, String valor, String nomeElemento) throws Exception {
		try {
			aguardarPorElemento(by);
			new Select(driver.findElement(by)).selectByValue(valor);

			logSucesso("Seleciona o value " + valor + " no elemento " + nomeElemento);
		} catch (Exception e) {
			logErro("N�o foi poss�vel selecionar o value " + valor + " no elemento " + by, e);
		}
	}

	// Seleciona op��o em combobox pela propriedade index passada por par�metro.
	public void selecionarElementoPorIndex(By by, int index, String nomeElemento) throws Exception {
		try {
			aguardarPorElemento(by);
			new Select(driver.findElement(by)).selectByIndex(index);

			logSucesso("Seleciona o index " + index + " no elemento " + nomeElemento);
		} catch (Exception e) {
			logErro("N�o foi poss�vel selecionar o index " + index + " no elemento " + by, e);
		}
	}

	// Seleciona op��o em combobox pelo texto passado por par�metro utilizando
	// comando javascript.
	public void selecionarElementoPorTextoComandoJavaScript(By by, String script, String texto, String nomeElemento)
			throws Exception {
		try {
			pausar(2000);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript(script); // exemplo de script:
										// document.getElementById('form').style=''
			new Select(driver.findElement(by)).selectByVisibleText(texto);

			logSucesso("Seleciona o texto " + texto + " no elemento " + nomeElemento);
		} catch (Exception e) {
			logErro("N�o foi poss�vel selecionar o texto " + texto + " no elemento " + by, e);
		}
	}

	// Seleciona op��o em combobox pela propriedade value passada por par�metro
	// utilizando comando javascript.
	public void selecionarElementoPorValueComandoJavaScript(By by, String script, String valor, String nomeElemento)
			throws Exception {
		try {
			pausar(2000);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript(script); // exemplo de script:
										// document.getElementById('form').style=''
			new Select(driver.findElement(by)).selectByValue(valor);

			logSucesso("Seleciona o value " + valor + " no elemento " + nomeElemento);
		} catch (Exception e) {
			logErro("N�o foi poss�vel selecionar o value " + valor + " no elemento " + by, e);
		}
	}

	// Seleciona op��o em combobox pela propriedade index passada por par�metro
	// utilizando comando javascript.
	public void selecionarElementoPorIndexComandoJavaScript(By by, String script, int index, String nomeElemento)
			throws Exception {
		try {
			pausar(2000);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript(script); // exemplo de script:
										// document.getElementById('form').style=''
			new Select(driver.findElement(by)).selectByIndex(index);

			logSucesso("Seleciona o index " + index + " no elemento " + nomeElemento);
		} catch (Exception e) {
			logErro("N�o foi poss�vel selecionar o index " + index + " no elemento " + by, e);
		}
	}

	// Verifica se o texto passado por par�metro n�o esta presente na
	// propriedade �text� do componente em quest�o.
	public void textoNaoPresentePorElemento(By by, String textoEsperado) throws Exception {
		try {
			AssertJUnit.assertFalse(driver.findElement(by).getText().contains(textoEsperado));
			logSucesso("Valida que o texto " + textoEsperado + " n�o esteja presente no elemento " + by);
		} catch (Exception e) {
			logErro("O texto " + textoEsperado + " est� presente na propriedade 'text' do elemento " + by, e);
		}
	}

	//// Verifica se o texto passado por par�metro esta presente na propriedade
	//// �text� do componente em quest�o.
	public void textoPresentePorElemento(By by, String textoEsperado) throws Exception {
		try {
			AssertJUnit.assertEquals(textoEsperado, driver.findElement(by).getText());
			logSucesso("Valida que o texto " + textoEsperado + " esteja presente no elemento " + by);
		} catch (Exception e) {
			logErro("O texto " + textoEsperado + " n�o est� presente na propriedade 'text' do elemento " + by, e);
		}
	}

	// Verifica se o componente em quest�o est� selecionado (checkbox por
	// exemplo).
	public void validarComponenteSelecionado(By by, String nomeElemento) throws Exception {
		try {
			AssertJUnit.assertTrue(driver.findElement(by).isSelected());
			logSucesso("Valida que o elemento " + nomeElemento + " esteja selecionado");
		} catch (Exception e) {
			logErro("O elemento " + nomeElemento + " n�o est� selecionado", e);
		}
	}

	// Verifica se o componente em quest�o est� habilitado.
	public void validarComponenteHabilitado(By by, String nomeElemento) throws Exception {
		try {
			AssertJUnit.assertTrue(driver.findElement(by).isEnabled());
			logSucesso("Valida que o elemento " + nomeElemento + " esteja habilitado");
		} catch (Exception e) {
			logErro("O elemento " + nomeElemento + " n�o est� habilitado", e);
		}
	}

	// Verifica se o componente em quest�o n�o est� selecionado (checkbox por
	// exemplo).
	public void validarComponenteNaoSelecionado(By by, String nomeElemento) throws Exception {
		try {
			AssertJUnit.assertFalse(driver.findElement(by).isSelected());
			logSucesso("Valida que o elemento " + nomeElemento + " n�o esteja selecionado");
		} catch (Exception e) {
			logErro("O elemento " + nomeElemento + " est� selecionado", e);
		}
	}

	// Verifica se o componente em quest�o n�o est� habilitado.
	public void validarComponenteDesabilitado(By by, String nomeElemento) throws Exception {
		try {
			AssertJUnit.assertFalse(driver.findElement(by).isEnabled());
			logSucesso("Valida que o elemento " + nomeElemento + " n�o esteja habilitado");
		} catch (Exception e) {
			logErro("O elemento " + nomeElemento + " est� habilitado", e);
		}
	}

	/*
	 * Ler dois arquivos de texto, verifica as diferen�as do primeiro para o segundo
	 * e do segundo para o primeiro arquivo e valida se o conte�do dos arquivos s�o
	 * iguais.
	 */
	public void validarExisteDiferencaEntreDoisArquivosTxt(String primeiroArquivo, String segundoArquivo)
			throws Exception {

		List<String> diferencaPrimeiroParaSegundo = new ArrayList<String>();
		List<String> diferencaSegundoParaPrimeiro = new ArrayList<String>();

		List<String> linhasA = Files.readAllLines(Paths.get(primeiroArquivo));
		List<String> linhasB = Files.readAllLines(Paths.get(segundoArquivo));

		diferencaPrimeiroParaSegundo.addAll(linhasA);
		diferencaPrimeiroParaSegundo.removeAll(linhasB);

		diferencaSegundoParaPrimeiro.addAll(linhasB);
		diferencaSegundoParaPrimeiro.removeAll(linhasA);

		if (linhasA.equals(linhasB)) {
			logSucesso("Os dois arquivos de texto s�o iguais.");
		} else {

			// Log4j
			Logger log = Logger.getLogger(getClass());
			log.error("Os dois arquivos de texto n�o s�o iguais. A diferen�a do primeiro para o segundo arquivo �: "
					+ diferencaPrimeiroParaSegundo + ", a diferen�a do segundo para o primeiro arquivo �: "
					+ diferencaSegundoParaPrimeiro);

			// Extent Reports -> Adiciona screenshot ao relat�rio
			String imagem = Utils.screenShot(DataDriven.nomeCaso);
			logger.log(LogStatus.FAIL,
					"Os dois arquivos de texto n�o s�o iguais. A diferen�a do primeiro para o segundo arquivo �: "
							+ diferencaPrimeiroParaSegundo + ", a diferen�a do segundo para o primeiro arquivo �: "
							+ diferencaSegundoParaPrimeiro + logger.addScreenCapture(imagem));

			// Finaliza o teste
			report.endTest(logger);

			// Acrescenta o arquivo HTML com todos os testes finalizados
			report.flush();

			// Adiciona o relat�rio .pdf de erro
			gerarRelatorioPDFErro();

			throw new Exception();
		}
	}

	// Valida se na URL atual cont�m o texto (trecho da URL).
	public void validarURLAtualContemString(String trechoURL) throws Exception {
		try {
			AssertJUnit.assertTrue(driver.getCurrentUrl().contains(trechoURL));
			logSucesso("Valida que a URL atual " + driver.getCurrentUrl() + " contenha o trecho " + trechoURL);
		} catch (Exception e) {
			logErro("A URL atual " + driver.getCurrentUrl() + " n�o cont�m o trecho " + trechoURL, e);
		}
	}

	// Retorna � tela anterior.
	public void voltarTelaAnterior() throws Exception {
		try {
			driver.navigate().back();
			logSucesso("Retorna � tela anterior. A URL atual agora � " + driver.getCurrentUrl());
		} catch (Exception e) {
			logErro("N�o foi poss�vel retornar � tela anterior. A URL atual � " + driver.getCurrentUrl(), e);
		}
	}

	// Loga no SISGR (usu�rio com acesso ao Sistema)
	public void logarSISGRUsuarioComAcesso() throws Exception {

		try {
			acessarURL(DataDriven.url);
			/*
			 * escreverTexto(By.id("usuario"), DataDriven.usuario, "Usuario");
			 * escreverTexto(By.id("senha"), DataDriven.senha, "Senha");
			 */
			clicarElemento(By.cssSelector("a > img[name=\"ok\"]"), "Bot�o OK");
			elementoEstaPresente(By.linkText(DataDriven.nomeSistema));

			logSucesso("Login no SISGR com sucesso, usu�rio com acesso ao sistema " + DataDriven.nomeSistema);
		} catch (Exception e) {
			logErro("N�o foi poss�vel validar o acesso ao SISGR, sistema " + DataDriven.nomeSistema
					+ "com este usu�rio/senha", e);
		}
	}

	// Loga no SISGR (usu�rio sem acesso ao Sistema)
	public void logarSISGRUsuarioSemAcesso() throws Exception {

		try {
			acessarURL(DataDriven.url);
			/*
			 * escreverTexto(By.id("usuario"), DataDriven.usuario, "Usuario");
			 * escreverTexto(By.id("senha"), DataDriven.senha, "Senha");
			 */
			clicarElemento(By.cssSelector("a > img[name=\"ok\"]"), "Bot�o OK");
			elementoNaoEstaPresente(By.linkText(DataDriven.nomeSistema));

			logSucesso("Login no SISGR com sucesso, usu�rio sem acesso ao sistema " + DataDriven.nomeSistema);
		} catch (Exception e) {
			logErro("N�o foi poss�vel validar o acesso ao SISGR com usu�rio sem permiss�o ao sistema "
					+ DataDriven.nomeSistema, e);
		}
	}

	public static String gerarDataHoraAtual() {

		// Configura objeto do tipo Date com o padr�o yyyyMMdd
		String cData = "yyyyMMdd";
		DateFormat formataData = new SimpleDateFormat("yyyyMMddHHmm");
		Date dDate = new Date();
		cData = formataData.format(dDate);

		return cData;
	}

	public static String obterVersaoBrowser() {
		Capabilities capabilities = ((RemoteWebDriver) driver).getCapabilities();
		Object strBrowserVersion = capabilities.getVersion();
		if (strBrowserVersion == "") {
			strBrowserVersion = capabilities.getCapability("browserVersion");
		} else {
			return (String) strBrowserVersion;
		}
		return (String) strBrowserVersion;
	}

	public static String obterNomeBrowser() {
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = cap.getBrowserName().toLowerCase();

		return browserName;
	}
	
}

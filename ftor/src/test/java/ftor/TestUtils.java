package ftor;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

/*************************************************************************************************************
 * TestUtils -> Classe respons�vel por conter o atributo driver, da interface WebDriver 
 * (API do Selenium WebDriver) e as chamadas aos m�todos de inicializa��o e t�rmino da inst�ncia do navegador. 
 * Cont�m as anota��es @BeforeMethod e @AfterMethod do TestNG.
 *************************************************************************************************************/
public class TestUtils {

	public static WebDriver driver;
	public static Utils util = new Utils();
	public static ReportLog reportlog = new ReportLog();
	
	@Parameters({ "browser", "platform", "version" })
	@BeforeMethod
	public static void defineBrowser(String browser, String platform, String version) {
		DesiredCapabilities extraCapabilities = new DesiredCapabilities();
		extraCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		extraCapabilities.setBrowserName(browser);
		
		// Verifica a plataforma
		if ("windows".equals(platform)) {
			extraCapabilities.setPlatform(Platform.WINDOWS);
		} else if ("linux".equals(platform)) {
			extraCapabilities.setPlatform(Platform.LINUX);
		} else if ("mac".equals(platform)) {
			extraCapabilities.setPlatform(Platform.MAC);
		}
		
		// Verifica a vers�o do navegador
		extraCapabilities.setVersion(version);
		
		// Define o browser
		switch (browser) {
		case "firefox-portable":
			Browser.executarComFirefoxPortable44();
			break;
		
		case "chrome":
			Browser.executarComChrome();
			break;
			
		case "chrome-headless":
			Browser.executarComChrome_Headless();
			break;	
		
		case "phantom-js-2.11":
			Browser.executarComPhantomJS_2_11();
			break;	
	}	
}

	@AfterMethod
	public void tearDown() throws Exception {
		
		/******************************************************************
		  --> Finaliza a grava��o de v�deo
		  --> Adiciona imagem ao campo'Capturas de Tela' do log do IBM RQM; 
		  --> Finaliza a inst�ncia do navegador
		 ******************************************************************/
		ReportLog.finalizarGravacaoVideoExecucaoCasoTeste();
		util.gravarTelaRQM();
		Browser.fecharInstancia();
	}
}

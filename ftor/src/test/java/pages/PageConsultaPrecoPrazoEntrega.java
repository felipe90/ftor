package pages;

import elementspage.ElementsPageConsultaPrecoPrazoEntrega;
import ftor.DataDriven;
import ftor.Utils;

public class PageConsultaPrecoPrazoEntrega {

	private ElementsPageConsultaPrecoPrazoEntrega elementos = new ElementsPageConsultaPrecoPrazoEntrega();
    Utils util = new Utils();
    
    public void acessarURLPrecoPrazoEntrega() throws Exception
    {
    	util.acessarURL(DataDriven.url);
    }
    
    public void informarCepOrigem() throws Exception
    {
    	util.escreverTexto(elementos.GetCepOrigem(), DataDriven.cepOrigem, "CEP Origem"); 
    }

    public void informarCepDestino() throws Exception
    {
        util.escreverTexto(elementos.GetCepDestino(), DataDriven.cepDestino, "CEP destino");
    }

    public void selecionarTipoServico() throws Exception
    {
        util.selecionarElementoPorTexto(elementos.GetTipoServico(), DataDriven.tipoServico, "Tipo de Servi�o");
    }

    public void clicarBotaoEnviar() throws Exception
    {
        util.clicarElemento(elementos.GetEnviar(), "Bot�o Enviar");
    }
}


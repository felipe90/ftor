package pages;

import ftor.DataDriven;
import ftor.Utils;

public class PageResultadoConsultaPrecoPrazoEntrega {

	Utils util = new Utils();
	
    public void ValidarConsultaPrecoPrazoEntrega() throws Exception
    {
    	String urlResultadoConsulta = "http://www2.correios.com.br/sistemas/precosPrazos/prazos.cfm";
    	util.alterarJanelaPorURL(urlResultadoConsulta);
        util.buscarPorTextoNaPagina(DataDriven.tipoServico);
    }
}
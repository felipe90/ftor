package elementspage;

import org.openqa.selenium.By;

public class ElementsPageConsultaPrecoPrazoEntrega {

	private By TxtCepOrigem = By.name("cepOrigem");
    private By TxtCepDestino = By.name("cepDestino");
    private By CmbTipoServico = By.name("servico");
    private By BtnEnviar = By.name("Calcular");

    public By GetCepOrigem()
    {
        return TxtCepOrigem;
    }

    public By GetCepDestino()
    {
        return TxtCepDestino;
    }

    public By GetTipoServico()
    {
        return CmbTipoServico;
    }

    public By GetEnviar()
    {
        return BtnEnviar;
    }  
}